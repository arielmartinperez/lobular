# Lobular

<strong>[EN]</strong>

Lobular is a typeface that looks like cross-sections of kidneys or the transversal section of an intestinal tract. It creates dark masses on the page's surface, in which light is ensnared when it enters the core of the letters. One of its main features is its entirely flat inktraps (the only straight lines in the typeface), which don't have the practical purpose of avoiding the effects of bleeding in print, but rather a structural role for drawing the counters. Its design also recalls that of bubble letters, commonly seen in graffiti tags in many places around the world. All in all, designing Lobular was a matter of solving topological problems (keeping curves and gaps consistent), and sometimes this constraint was also used as an inspiration to create experimental letterforms.

Lobular has been created by Ariel Martín Pérez (www.appliedmetaprojects.com - contact@appliedmetaprojects.com) and released under the SIL Open Font Licence 1.1 in 2021. Lobular is distributed by Tunera Type Foundry (www.tunera.xyz).

To know how to use this typeface, please read the FAQ (http://www.tunera.xyz/faq/)

<strong>[FR]</strong>

Lobular est un caractère typographique dont l'aspect rapelle celui d'une section longitudinale d'un rein ou d'une fine tranche transversale d'un intestin. Il crée des masses foncées sur la page, dans lesquelles la lumière est capturée quand elle s'introduit dans le noyau des lettres. Une de ses caractéristiques principales ce sont ses pièges d'encre (les seules lignes droites de cette fonte), qui n'ont pas l'objectif pratique d'éviter les effets de l'excès d'encre mais qui ont plutôt un rôle structurel pour le dessin des contreformes. Son dessin rappelle également celui des lettres "bulle", courantes dans les tags de graffiti autour du monde. En conclusion, la conception du Lobular est une affaire de résolution de problèmes topologiques (assurer que les courbes et les ouvertures soient cohérentes), et parfois cette contrainte a servi d'inspiration pour la création de formes de lettres expérimentales.

Lobular a été créé par Ariel Martín Pérez (www.appliedmetaprojects.com - contact@appliedmetaprojects.com) et publié sous la licence SIL Open Font License 1.1 en 2021. Lobular est distribué par Tunera Type Foundry (www.tunera.xyz).

Pour savoir comment utiliser cette fonte, veuillez lire la FAQ (http://www.tunera.xyz/faq-2/)

<strong>[ES]</strong>

Lobular es un tipo de letra cuyo aspecto recuerda el de la sección longitudinal de un riñón o de la sección transversal de un tracto intestinal. Genera masas oscuras en la superficie de la página, en las que la luz queda atrapada al entrar en el núcleo de las letras. Una de sus características principales es el uso de trampas de tinta totalmente planas (las únicas líneas rectas del tipo de letra), que no tienen el objetivo práctico de evitar los efectos del exceso de tinta en la impresión, sino un papel estructural para la construcción de las contraformas. Su diseño también se refiere al de los letras de tipo "burbuja", que son comunes en pintadas de todo el mundo. En conclusión, diseñar Lobular fue una cuestión de resolver problemas topológicos (conseguir que las curvas y las aberturas sean coherentes), y a veces esta limitación fue empleada como inspiración para crear formas de letras experimentales.

Lobular ha sido creado por Ariel Martín Pérez (www.appliedmetaprojects.com - contact@appliedmetaprojects.com) y publicado bajo la licencia SIL Open Font License 1.1 en 2021. Lobular es distribuido por Tunera Type Foundry (www.tunera.xyz).

Para saber cómo usar este tipo de letra, lea las preguntas frecuentes (http://www.tunera.xyz/preguntas-frecuentes/)

## Specimen

![specimen1](documentation/specimen/lobular-specimen-01.png)
![specimen1](documentation/specimen/lobular-specimen-02.png)
![specimen1](documentation/specimen/lobular-specimen-03.png)

## License

Lobular is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at
http://scripts.sil.org/OFL

## Repository Layout

This font repository follows the Unified Font Repository v2.0,
a standard way to organize font project source files. Learn more at
https://github.com/unified-font-repository/Unified-Font-Repository
